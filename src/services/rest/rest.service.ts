import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ToastController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { Network } from '@ionic-native/network/ngx';


@Injectable({
  providedIn: 'root'
})
export class RestService {

  private apiUrl = 'https://restcountries.eu/rest/v2/all';
  private RestUrl = 'https://comparons.ovh/app.php/cservice';



  public User:any={
    username:'',
    password:'',
    session:false
  }

  public Selection:any={
    zoneId:48,
    villeId:false,
    marketId:false
  }

  constructor(public http: HttpClient,
    private network:Network,
    public toastController: ToastController,
    ) {

      this.watchConnection();

      this.Selection = {
        zoneId : 48,
        villeId : '',
        marketId : ''
        //villeId : '361',
        //marketId : '91'
      }  
  }

  async presentToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

    isConnected(){
      if(this.network.type == 'none')
      {
        this.presentToast('Aucune connexion Internet');
      }
      
    }

    getConnectionNetwork()
    {
      if(this.network.type == 'none')
      {
        return false;
      }
      return true;
    }

    watchConnection(){
      // watch network for a disconnection
      let disconnectSubscription = this.network.onDisconnect().subscribe(() => {
        this.presentToast('Aucune connexion');
      });

      let connectSubscription = this.network.onConnect().subscribe(() => {
        this.presentToast('De nouveau connecter à internet');
        // We just got a connection but we need to wait briefly
         // before we determine the connection type. Might need to wait.
        // prior to doing any api requests as well.
        /*setTimeout(() => {
          if (this.network.type === 'wifi') {
            console.log('we got a wifi connection, woohoo!');
          }
        }, 3000);*/
      });
    }

    handlerSelection(){
      return !this.Selection.marketId || !this.Selection.villeId;
    }

  serialize = function(obj) {
    var str = [];
    for(var p in obj)
      if (obj.hasOwnProperty(p)) {
          if(typeof obj[p] == 'object')
          {
              
                str.push(encodeURIComponent(p) + "=" + encodeURIComponent(JSON.stringify(obj[p])));
          }
          else
          {
                str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
          }
      }
      
    return str.join("&");
  }  
  
  defaultZone = 48;
  
  
    
  post(method,data): Observable<{}>
  {
    this.isConnected();

    
    let headers = {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'};
    if(this.User.session)
    {
        data['cookie'] = this.User.session;
    }

    return this.http.post(this.RestUrl+method,this.serialize(data),{headers: headers}).pipe(
      //map(this.extractData),
      //catchError(this.handleError)
    );

  }  

  /*getCountries(): Observable<string[]> {
    return this.http.get(this.apiUrl).pipe(
      map(this.extractData),
      catchError(this.handleError)
    );
  }*/
  
  private extractData(res: Response) {
    let body = res;

    return body || { };
  }
  
  private handleError (error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const err = error || '';
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }


}
