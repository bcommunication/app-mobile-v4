import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Device } from '@ionic-native/device/ngx';


declare var local_zones;

declare var local_rayons;
declare var pays;

@Injectable({
  providedIn: 'root'
})
export class DataService {

  deviceInfo : any={};

  countries: any =[];
  
  
  keysZone:any;
  keysVille:any;
  keysMarket:any;
  
  keysRayons:any;
  
  
  dataZone:any[] = [];
  dataVille:any[] = [];
  dataMarket:any[] = [];
  
  dataRayons:any[] = [];
  dataCategories:any[] = [];
  
  
    constructor(
      public http: HttpClient,
      private device: Device
  
      ) {
      this.keysZone = Object.keys(local_zones);
      this.keysRayons = Object.keys(local_rayons);
      this.countries = pays;
    }
  
  
    getCountries() : Promise<any>{
      return new Promise((resolve,reject)=>
      {
        let items : any 	= [];
  
        this.countries.forEach(element => {
          items.push({
            alpha2Code:element[2].toLowerCase(),
            name:element[3]
          })
        });
        resolve( items );
  
      })
  
    }
  
  getZoneName(){
    this.keysZone.forEach(k => {
      //console.log(local_zones[k]);
      this.dataZone.push({
        'nom':local_zones[k]['nom'],
        'code':local_zones[k]['code'],
        'id':k
      });
    });
    return this.dataZone;
  }
  
  getVilleParZone(zoneId:any){
    this.keysVille = [];
    this.dataVille = [];
    this.keysVille = Object.keys(local_zones[zoneId].villes);
  
    this.keysVille.forEach(k => {
      //console.log(local_zones[k]);
      this.dataVille.push({
        'nom':local_zones[zoneId].villes[k]['nom'],
        'id':k
      });
    });
    return this.dataVille;
  }
  
  getMarketParVille(zoneId, villeId){
    this.dataMarket = [];
    this.dataMarket = local_zones[zoneId].villes[villeId]['markets'];
  
    return this.dataMarket;
  }
  
  
  getRayonName(){
    this.dataRayons = [];
    this.keysRayons.forEach(k => {
      //console.log(local_zones[k]);
      this.dataRayons.push({
        'nom':local_rayons[k]['nom'],
        'id':k
      });
    });
    return this.dataRayons;
  }
  
  getCategories(rayonId){
    this.dataCategories = [];
    let data:any[] = Object.keys(local_rayons[rayonId]['categories']);
      data.forEach(k=>{
        let splitData:any[] = local_rayons[rayonId]['categories'][k].split(':');
        this.dataCategories.push({
          'nom':splitData[1],
          'id':splitData[0]
        })
      })
    return this.dataCategories;
  }
  
  getDeviceInfo()
  {
    this.deviceInfo = {
      serial:this.device.serial,
      model:this.device.model,
      uuid:this.device.uuid,
      platform:this.device.platform
    }
  }
}
