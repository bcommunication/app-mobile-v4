import { Platform } from '@ionic/angular';

import { ConnectionOptions, createConnection } from 'typeorm';

import { ModelCache } from 'src/entities/cache';
import { Injectable } from '@angular/core';

@Injectable()
export class OrmService {

  private _DB_NAME : string 		= "__comparons";

  public models = [
    ModelCache
  ] 

  constructor(private platform: Platform) { }

  async prepare() {
    let dbOptions: ConnectionOptions;

    if (this.platform.is('cordova')) {

      dbOptions = {
        type: 'cordova',
        database: this._DB_NAME,
        location: 'default',
        cache:true
      };
    } else {

      dbOptions = {
        type: 'sqljs',
        location: 'browser',
        autoSave: true,
        cache:true
      };
    }

    Object.assign(dbOptions, {
      logging: ['error', 'query', 'schema'],
      synchronize: true,
      entities: [
        ModelCache
      ]
    });
    
    await createConnection(dbOptions);

  }

}