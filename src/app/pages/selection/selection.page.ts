import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/services/data/data.service';
import { RestService } from 'src/services/rest/rest.service';

@Component({
  selector: 'app-selection',
  templateUrl: './selection.page.html',
  styleUrls: ['./selection.page.scss'],
})
export class SelectionPage implements OnInit {

  zoneName:any;
  villeName:any;
  marketName:any;
  selection = {
   zoneId : 48,
   villeId : '',
   marketId : ''
 }

  constructor(
    public data:DataService,
    public rest:RestService
    ) { }

  ngOnInit() {


        this.selection = this.rest.Selection;

        this.villeName = this.data.getVilleParZone(48);
        console.log(this.villeName);

        if(this.rest.Selection.villeId)
        {
          this.marketName = this.data.getMarketParVille(this.rest.Selection.zoneId, this.rest.Selection.villeId);
    
        }
      }
    
      villeChange(){
        this.marketName = this.data.getMarketParVille(this.selection.zoneId, this.selection.villeId);
      }
    
      marketchange(){
        this.rest.Selection = {
          zoneId:this.selection.zoneId,
          villeId:this.selection.villeId,
          marketId:this.selection.marketId
        }
    
      }
    
      valider()
      {
        console.log('ok');
      }
  }


