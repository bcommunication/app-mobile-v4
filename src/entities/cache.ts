import { Column, Entity, PrimaryColumn } from "typeorm";

@Entity()
export class ModelCache{

    @PrimaryColumn()
    url:string;
    
    @Column()
    base64:string;
}